package com.inmortal.wallpaperstore;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andreseko.SweetAlert.SweetAlertDialog;
import com.google.gson.Gson;
import com.inmortal.wallpaperstore.utill.ApiURL;
import com.inmortal.wallpaperstore.utill.NetworkCall;
import com.inmortal.wallpaperstore.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class more_wallpaper_category extends AppCompatActivity implements NetworkCall.MyNetworkCallBack, Adapter_category_more.ReturnView {
    Progress progress;
    NetworkCall networkCall;
    SharedPreferences mSharedPreference;

    private RecyclerView rv, category_recy;
    ArrayList<Model_category_list> arrCategory_list = new ArrayList<>();
    String Category_id;
    Model_category_list model_category_list;
    Adapter_category_more adapter_category_more;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_wallpaper_category);

        progress = new Progress(more_wallpaper_category.this);
        networkCall = new NetworkCall(more_wallpaper_category.this, more_wallpaper_category.this);

        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        Category_id = (mSharedPreference.getString("category_id", ""));

        category_recy = (RecyclerView) findViewById(R.id.category_recycler);
        categoryData();

    }

    private void categoryData() {
        networkCall.NetworkAPICall(ApiURL.category_list, true);

    }


    @Override
    public void getAdapterView(View view, List objects, int position, int from) {
        Model_category_list model_category_list = arrCategory_list.get(position);
        TextView name = view.findViewById(R.id.category_txt);
        ImageView pic = view.findViewById(R.id.category_img);
        LinearLayout category = view.findViewById(R.id.category);
        category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Category_id = String.valueOf(model_category_list.getId());



                Intent intent = new Intent(more_wallpaper_category.this, catrgory_wallpapers.class);
                intent.putExtra("id", Category_id);
                startActivity(intent);
            }
        });

        model_category_list.getCategory();
        String category_name;

        category_name = model_category_list.getCategory_name();
        name.setText(category_name);
        String img;
        img = model_category_list.getCategory();
        if (!img.equals("")) {
            Picasso.with(getApplicationContext())
                    .load(img)
                    .into(pic);


        }

    }


    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.category_list:
                ion = (Builders.Any.B) Ion.with(more_wallpaper_category.this)
                        .load("GET", ApiURL.category_list);

                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {
            case ApiURL.category_list:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String status = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");


                    if (status.equals("true")) {

                        JSONArray jsonObject1 = jsonObject.getJSONArray("data");

                        for (int i = 0; i < jsonObject1.length(); i++) {
                            model_category_list = new Gson().fromJson(jsonObject1.optJSONObject(i).toString(), Model_category_list.class);
                            arrCategory_list.add(model_category_list);

                        }


                        adapter_category_more = new Adapter_category_more(arrCategory_list, more_wallpaper_category.this, R.layout.more_category_layout, this, 1);
                        category_recy.setHasFixedSize(true);
                        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
                        category_recy.setLayoutManager(layoutManager);
                        category_recy.setAdapter(adapter_category_more);


                    } else {


                        String fail_status = jsonObject.getString("success");

                        if (fail_status.equals("false")) {

                            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Wallpaper Store")
                                    .setContentText(msg)
                                    .show();


                            //  Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                        }
                        progress.dismiss();
                    }
                } catch (JSONException e1) {


                    Toast.makeText(more_wallpaper_category.this, "" + e1, Toast.LENGTH_SHORT).show();
                }

                break;
        }

    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }
}