package com.inmortal.wallpaperstore;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Dialog;
import android.app.WallpaperManager;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.andreseko.SweetAlert.SweetAlertDialog;
import com.google.gson.Gson;
import com.inmortal.wallpaperstore.utill.ApiURL;
import com.inmortal.wallpaperstore.utill.NetworkCall;
import com.inmortal.wallpaperstore.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import soup.neumorphism.NeumorphCardView;

public class more_trending extends AppCompatActivity implements NetworkCall.MyNetworkCallBack,Adapter_trending_all_wallpapers.ReturnView{
    String Category_id;
    Progress progress;
    NetworkCall networkCall;
    ArrayList<Model_trending_wall> arrTrend = new ArrayList<>();
    RecyclerView wallRecycler;
    File outFile;
    Bitmap wal_set;
    String img_regex = "([^\\s]+(\\.(?i)(jpe?g|png|gif|bmp))$)";
    Model_trending_wall model_trending_wall;
    Adapter_trending_all_wallpapers adapter_trending_all_wallpapers;
    String[] appPermission = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    private static final int PERMISSION_REQUEST_CODE = 1240;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_trending);
        progress = new Progress(more_trending.this);
        networkCall = new NetworkCall(more_trending.this, more_trending.this);
        wallRecycler = findViewById(R.id.trending_wallpapers);
        loadDate();
    }

    private void initApp() {
    }

    private boolean checkAndRequestPermission() {
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String perm : appPermission) {
            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(perm);
            }
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSION_REQUEST_CODE
            );
            return false;
        }
        return true;
    }

    private void loadDate() {
        networkCall.NetworkAPICall(ApiURL.trending_wall, true);
    }

    @Override
    public void getTrendAllwall(View view, List objects, int position, int from) {
        ImageView imgV = (ImageView) view.findViewById(R.id.image_view);
        imgV.setDrawingCacheEnabled(true);
        NeumorphCardView img_download = (NeumorphCardView) view.findViewById(R.id.btn_download);
        NeumorphCardView img_share = (NeumorphCardView) view.findViewById(R.id.btnsetWallpaper);

         model_trending_wall = arrTrend.get(position);

        String wall;
        wall = model_trending_wall.getWallpaper();

        if (!imgV.equals("")) {

            Picasso.with(getApplicationContext())

                    .load(wall)
                    .into(imgV);
        }

        img_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(more_trending.this, "Dawnloading....", Toast.LENGTH_SHORT).show();
//                            progress.show();
                Picasso.with(more_trending.this)
                        .load(wall)

                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                try {
                                    String root = Environment.getExternalStorageDirectory().toString();
                                    File myDir = new File(root + "/DCIM/Camera");

                                    if (!myDir.exists()) {
                                        myDir.mkdirs();
                                    }
                                    //  String NewDate = new Date().toString();
                                    Random generator = new Random();
                                    int n = 10000;
                                    n = generator.nextInt(n);
                                    String fname = "Image-" + n + ".jpg";

                                    // String name = new Date().toString()+".jpg";
                                    // String  newIMG = name.replaceAll(" ","");

                                    myDir = new File(myDir, fname);
                                    FileOutputStream out = new FileOutputStream(myDir);
                                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
//                                                      progress.dismiss();
                                    Toast.makeText(more_trending.this, "Dawnloading complete", Toast.LENGTH_SHORT).show();

                                    out.flush();
                                    out.close();
                                } catch (Exception e) {
                                    Log.e("Exe", String.valueOf(e));

                                    if (e != null) {

                                        Toast.makeText(more_trending.this, "OOPs! Please Try Again", Toast.LENGTH_SHORT).show();

                                    }
// else{
//
//                                      progress.dismiss();
//                                      // some action
//                                  }
                                }

                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {
                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {
                            }


                        });


            }

        });


        img_share.setOnClickListener(vieww -> {
            try {

                Toast.makeText(more_trending.this, "Please Wait....", Toast.LENGTH_SHORT).show();
                WallpaperManager myWallpaperManager = WallpaperManager.getInstance(getApplicationContext());

                myWallpaperManager.setWallpaperOffsetSteps(0, 0);

                Drawable drawable = imgV.getDrawable();


                //or if the above line of code doesn't work try fetching the image from your array list
                Toast.makeText(more_trending.this, "Wallpaper set Successfully", Toast.LENGTH_SHORT).show();

                arrTrend.get(position);
                Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                myWallpaperManager.setBitmap(bitmap);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = false;


        });

        imgV.setOnClickListener(v -> {
            Drawable drawable = imgV.getDrawable();
            //or if the above line of code doesn't work try fetching the image from your array list
            arrTrend.get(position);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Dialog dialogg = new Dialog(more_trending.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
            dialogg.setContentView(R.layout.activity_full_iimage);
            CropImageView Full = dialogg.findViewById(R.id.iv_background);
            Full.setImageBitmap(bitmap);

            CircleImageView setWall = dialogg.findViewById(R.id.button_setwallpaper);
            setWall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(more_trending.this, "Please Wait....", Toast.LENGTH_SHORT).show();

                    wal_set = Full.getCroppedImage();
                    WallpaperManager wallpaperManager = WallpaperManager.getInstance(getApplicationContext());

                    wallpaperManager.setWallpaperOffsetSteps(0, 0);
                    try {

                        wallpaperManager.setBitmap(wal_set);
                        Toast.makeText(more_trending.this, "Wallpaper set Successfully", Toast.LENGTH_SHORT).show();


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    dialogg.dismiss();
                }
            });

            Window window = dialogg.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();
            wlp.gravity = Gravity.CENTER;

            dialogg.show();
            dialogg.setCanceledOnTouchOutside(true);
        });


    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.trending_wall:
                ion = (Builders.Any.B) Ion.with(more_trending.this)
                        .load("GET", ApiURL.trending_wall);

                break;
        }

        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {
            case ApiURL.trending_wall:
                try {
                    JSONObject wallpaper_list = new JSONObject(jsonstring.toString());

                    String status = wallpaper_list.getString("success");
                    String msg = wallpaper_list.getString("message");

                    if (status.equals("true")) {

                        JSONArray withdrawHistoryList = wallpaper_list.getJSONArray("data");
                        for (int i = 0; i < withdrawHistoryList.length(); i++) {
                            model_trending_wall = new Gson().fromJson(withdrawHistoryList.optJSONObject(i).toString(), Model_trending_wall.class);
                            arrTrend.add(model_trending_wall);

                            //   category_id = withdrawHistoryList.getJSONObject(i).getString("category_id");

                        }

                        adapter_trending_all_wallpapers = new Adapter_trending_all_wallpapers(arrTrend, more_trending.this, R.layout.list_data, this, 1);

                        wallRecycler.setHasFixedSize(true);
                        wallRecycler.setLayoutManager(new LinearLayoutManager(this));

                        wallRecycler.setAdapter(adapter_trending_all_wallpapers);

                    } else {

                        String status_fail = wallpaper_list.getString("success");
                        if (status_fail.equals("false")) {
                            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Wallpaper Store")
                                    .setContentText(msg)
                                    .show();


                            progress.dismiss();
                        }
                    }
                } catch (JSONException e1) {
                    Toast.makeText(more_trending.this, "" + e1, Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }
}