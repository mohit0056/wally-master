package com.inmortal.wallpaperstore;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.HashSet;
import java.util.List;

public class Adapter_Trending_wall extends RecyclerView.Adapter<Adapter_Trending_wall.TruckHolder> {
    private List list;
    private Context context;
    private int layout;
    private HashSet<String> hashSet = new HashSet<String>();
    ReturnView returnView;
    int from;

    public interface ReturnView {
        void getTrendingwall(View view, List objects, int position, int from);
    }
    public Adapter_Trending_wall(List list1, Context context, int layout, ReturnView returnView, int from) {
        this.list = list1;
        this.context = context;
        this.layout = layout;
        this.returnView = returnView;
        this.from = from;
    }
    @NonNull
    @Override
    public TruckHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.latest_wall_layout,parent,false);
        return new TruckHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TruckHolder holder, int position) {
        returnView.getTrendingwall(holder.itemView, list, position, from);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class TruckHolder extends RecyclerView.ViewHolder {
        public TruckHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
