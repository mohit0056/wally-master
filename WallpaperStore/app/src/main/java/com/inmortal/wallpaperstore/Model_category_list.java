package com.inmortal.wallpaperstore;
public class Model_category_list
{
    private int id;

    private String category;

    private String category_name;

    private String created_at;

    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return this.id;
    }
    public void setCategory(String category){
        this.category = category;
    }
    public String getCategory(){
        return this.category;
    }
    public void setCategory_name(String category_name){
        this.category_name = category_name;
    }
    public String getCategory_name(){
        return this.category_name;
    }
    public void setCreated_at(String created_at){
        this.created_at = created_at;
    }
    public String getCreated_at(){
        return this.created_at;
    }
}
