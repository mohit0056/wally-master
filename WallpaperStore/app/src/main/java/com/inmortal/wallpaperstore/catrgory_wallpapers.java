package com.inmortal.wallpaperstore;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Dialog;
import android.app.WallpaperManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.FileUtils;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.andreseko.SweetAlert.SweetAlertDialog;
import com.google.gson.Gson;
import com.inmortal.wallpaperstore.utill.ApiURL;
import com.inmortal.wallpaperstore.utill.NetworkCall;
import com.inmortal.wallpaperstore.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import soup.neumorphism.NeumorphCardView;
import soup.neumorphism.NeumorphImageButton;

public class catrgory_wallpapers extends AppCompatActivity implements NetworkCall.MyNetworkCallBack, Adapter_wall.ReturnView {
    String Category_id, viewCategory_id, viewCategoryWallpaper_id;
    Progress progress;
    NetworkCall networkCall;
    ArrayList<Model_wall_list> arrWall_list = new ArrayList<>();
    RecyclerView wallRecycler;
    File outFile;
    String View;
    Bitmap wal_set;
    String img_regex = "([^\\s]+(\\.(?i)(jpe?g|png|gif|bmp))$)";
    Model_wall_list model_wall_list;
    Adapter_wall adapter_wall;
    String[] appPermission = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    private static final int PERMISSION_REQUEST_CODE = 1240;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catrgory_wallpapers);
        progress = new Progress(catrgory_wallpapers.this);
        networkCall = new NetworkCall(catrgory_wallpapers.this, catrgory_wallpapers.this);

        Intent intentnum = getIntent();
        Category_id = intentnum.getStringExtra("id");
        wallRecycler = findViewById(R.id.wallpapers);
        loadDate();

        if (checkAndRequestPermission()) {
            initApp();
        }
    }

    private void initApp() {
    }

    private boolean checkAndRequestPermission() {
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String perm : appPermission) {
            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(perm);
            }
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSION_REQUEST_CODE
            );
            return false;
        }
        return true;
    }

    private void loadDate() {
        networkCall.NetworkAPICall("http://inmortaltech.com/Wallpaper-APIs/public/api/wallpaper", true);

    }


    @Override
    public void getWallView(View view, List objects, int position, int from) {
        ImageView imgV = (ImageView) view.findViewById(R.id.image_view);
        imgV.setDrawingCacheEnabled(true);
        NeumorphCardView img_download = (NeumorphCardView) view.findViewById(R.id.btn_download);
        NeumorphCardView img_share = (NeumorphCardView) view.findViewById(R.id.btnsetWallpaper);

        Model_wall_list model_wall_list = arrWall_list.get(position);

        String wall;
        wall = model_wall_list.getWallpaper();

        if (!imgV.equals("")) {

            Picasso.with(getApplicationContext())

                    .load(wall)
                    .into(imgV);
        }

        img_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(catrgory_wallpapers.this, "Dawnloading....", Toast.LENGTH_SHORT).show();
//                            progress.show();
                Picasso.with(catrgory_wallpapers.this)
                        .load(wall)

                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                try {
                                    String root = Environment.getExternalStorageDirectory().toString();
                                    File myDir = new File(root + "/DCIM/Camera");

                                    if (!myDir.exists()) {
                                        myDir.mkdirs();
                                    }
                                    //  String NewDate = new Date().toString();
                                    Random generator = new Random();
                                    int n = 10000;
                                    n = generator.nextInt(n);
                                    String fname = "Image-" + n + ".jpg";

                                    // String name = new Date().toString()+".jpg";
                                    // String  newIMG = name.replaceAll(" ","");

                                    myDir = new File(myDir, fname);
                                    FileOutputStream out = new FileOutputStream(myDir);
                                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
//                                                      progress.dismiss();
                                    Toast.makeText(catrgory_wallpapers.this, "Downloading complete", Toast.LENGTH_SHORT).show();

                                    out.flush();
                                    out.close();
                                } catch (Exception e) {
                                    Log.e("Exe", String.valueOf(e));

                                    if (e != null) {

                                        Toast.makeText(catrgory_wallpapers.this, "OOPs! Please Try Again", Toast.LENGTH_SHORT).show();

                                    }
// else{
//
//                                      progress.dismiss();
//                                      // some action
//                                  }
                                }

                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {
                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {
                            }


                        });


            }

        });

//
//        img_download.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //to get the image from the ImageView (say iv)
//                BitmapDrawable draw = (BitmapDrawable) imgV.getDrawable();
//                Bitmap bitmap = draw.getBitmap();
//                Toast.makeText(catrgory_wallpapers.this, "Saved Successfully", Toast.LENGTH_SHORT).show();
//                FileOutputStream outStream = null;
//                File sdCard = Environment.getExternalStorageDirectory();
//                File dir = new File(sdCard.getAbsolutePath() + "/YourFolderName");
//                dir.mkdirs();
//                String fileName = String.format(img_regex, System.currentTimeMillis());
//                outFile = new File(dir, fileName);
//                try {
//                    outStream = new FileOutputStream(outFile);
//                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
//                    outStream.flush();
//                    outStream.close();
//                    scanFile(catrgory_wallpapers.this, Uri.fromFile(outFile));
//
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        });


        img_share.setOnClickListener(vieww -> {
            try {

                Toast.makeText(catrgory_wallpapers.this, "Please Wait....", Toast.LENGTH_SHORT).show();
                WallpaperManager myWallpaperManager = WallpaperManager.getInstance(getApplicationContext());

                myWallpaperManager.setWallpaperOffsetSteps(0, 0);

                Drawable drawable = imgV.getDrawable();


                //or if the above line of code doesn't work try fetching the image from your array list
                Toast.makeText(catrgory_wallpapers.this, "Wallpaper set Successfully", Toast.LENGTH_SHORT).show();

                arrWall_list.get(position);
                Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                myWallpaperManager.setBitmap(bitmap);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = false;


        });

        imgV.setOnClickListener(v -> {
            Drawable drawable = imgV.getDrawable();
            //or if the above line of code doesn't work try fetching the image from your array list
            arrWall_list.get(position);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Dialog dialogg = new Dialog(catrgory_wallpapers.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
            dialogg.setContentView(R.layout.activity_full_iimage);
            CropImageView Full = dialogg.findViewById(R.id.iv_background);
            Full.setImageBitmap(bitmap);
            View = "1";
            viewCategory_id = arrWall_list.get(position).getCategory_id();
            viewCategoryWallpaper_id = String.valueOf(arrWall_list.get(position).getId());
            viewApi();
            CircleImageView setWall = dialogg.findViewById(R.id.button_setwallpaper);
            setWall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(catrgory_wallpapers.this, "Please Wait....", Toast.LENGTH_SHORT).show();

                    wal_set = Full.getCroppedImage();
                    WallpaperManager wallpaperManager = WallpaperManager.getInstance(getApplicationContext());

                    wallpaperManager.setWallpaperOffsetSteps(0, 0);
                    try {

                        wallpaperManager.setBitmap(wal_set);
                        Toast.makeText(catrgory_wallpapers.this, "Wallpaper set Successfully", Toast.LENGTH_SHORT).show();


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    dialogg.dismiss();
                }
            });

            Window window = dialogg.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();
            wlp.gravity = Gravity.CENTER;

            dialogg.show();
            dialogg.setCanceledOnTouchOutside(true);
        });


    }

    private void viewApi() {
        networkCall.NetworkAPICall(ApiURL.trending_view, true);

    }

    private static void scanFile(Context context, Uri imageUri) {
        Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        scanIntent.setData(imageUri);
        context.sendBroadcast(scanIntent);
    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case "http://inmortaltech.com/Wallpaper-APIs/public/api/wallpaper":
                ion = (Builders.Any.B) Ion.with(catrgory_wallpapers.this)
                        .load("POST", "http://inmortaltech.com/Wallpaper-APIs/public/api/wallpaper")
                        .setBodyParameter("category_id", Category_id);

                break;

            case ApiURL.trending_view:
                ion = (Builders.Any.B) Ion.with(catrgory_wallpapers.this)
                        .load("POST", ApiURL.trending_view)
                        .setBodyParameter("category_id", viewCategory_id)
                        .setBodyParameter("wallpaper_id", viewCategoryWallpaper_id)
                        .setBodyParameter("view", View);

                break;
        }

        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {
            case "http://inmortaltech.com/Wallpaper-APIs/public/api/wallpaper":
                try {
                    JSONObject wallpaper_list = new JSONObject(jsonstring.toString());

                    String status = wallpaper_list.getString("success");
                    String msg = wallpaper_list.getString("message");

                    if (status.equals("true")) {

                        JSONArray withdrawHistoryList = wallpaper_list.getJSONArray("data");
                        for (int i = 0; i < withdrawHistoryList.length(); i++) {
                            model_wall_list = new Gson().fromJson(withdrawHistoryList.optJSONObject(i).toString(), Model_wall_list.class);
                            arrWall_list.add(model_wall_list);

                            //   category_id = withdrawHistoryList.getJSONObject(i).getString("category_id");

                        }

                        adapter_wall = new Adapter_wall(arrWall_list, catrgory_wallpapers.this, R.layout.list_data, this, 1);

                        wallRecycler.setHasFixedSize(true);
                        wallRecycler.setLayoutManager(new LinearLayoutManager(this));

                        wallRecycler.setAdapter(adapter_wall);

                    } else {

                        String status_fail = wallpaper_list.getString("success");
                        if (status_fail.equals("false")) {
                            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Wallpaper Store")
                                    .setContentText(msg)
                                    .show();


                            progress.dismiss();
                        }
                    }
                } catch (JSONException e1) {
                    Toast.makeText(catrgory_wallpapers.this, "" + e1, Toast.LENGTH_SHORT).show();
                }

                break;

            case ApiURL.trending_view:
                try {
                    JSONObject wallpaper_view = new JSONObject(jsonstring.toString());
                    String status = wallpaper_view.getString("success");
                    String msg = wallpaper_view.getString("message");

                    if (status.equals("true")) {


                    } else {

                        String status_fail = wallpaper_view.getString("success");
                        if (status_fail.equals("false")) {
                            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Wallpaper Store")
                                    .setContentText(msg)
                                    .show();


                            progress.dismiss();
                        }
                    }
                } catch (JSONException e1) {
                    Toast.makeText(catrgory_wallpapers.this, "" + e1, Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }
}