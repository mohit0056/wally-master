package com.inmortal.wallpaperstore;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.app.Dialog;
import android.app.WallpaperManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andreseko.SweetAlert.SweetAlertDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.inmortal.wallpaperstore.utill.ApiURL;
import com.inmortal.wallpaperstore.utill.NetworkCall;
import com.inmortal.wallpaperstore.utill.Progress;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import soup.neumorphism.NeumorphImageButton;

public class MainActivity extends AppCompatActivity implements NetworkCall.MyNetworkCallBack, Adapter_main_viewPager.pageonClick, NavigationView.OnNavigationItemSelectedListener, Adapter_category.ReturnView, Adapter_latest_wall.ReturnView ,Adapter_Trending_wall.ReturnView{
    ViewPager viewPager;
    Progress progress;
    private DrawerLayout mDrawer;
    Bannerpojo bannerpojo;
    private RecyclerView rv, category_recy, latest_recy, trending_recy;
    ArrayList<Model_category_list> arrCategory_list = new ArrayList<>();
    ArrayList<Model_latest_wall> arrLatestwall = new ArrayList<>();
    ArrayList<Model_trending_wall> arrTrending = new ArrayList<>();
    String Category_id;
    Model_category_list model_category_list;
    Model_latest_wall model_latest_wall;
    Model_trending_wall model_trending_wall;
    Adapter_category adapter_category;
    Bitmap wal_set;
    Adapter_latest_wall adapter_latest_wall;
    Adapter_Trending_wall adapter_trending_wall;
    SharedPreferences mSharedPreference;
    NeumorphImageButton imageButton, img2;
    NetworkCall networkCall;
    public static ArrayList<Bannerpojo> listBanners = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progress = new Progress(MainActivity.this);
        networkCall = new NetworkCall(MainActivity.this, MainActivity.this);

        mSharedPreference = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        Category_id = (mSharedPreference.getString("category_id", ""));


        mDrawer = (DrawerLayout) findViewById(R.id.drawerlayout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.vNavigation);
        navigationView.setNavigationItemSelectedListener((NavigationView.OnNavigationItemSelectedListener) this);
        category_recy = (RecyclerView) findViewById(R.id.category_recycler);
        latest_recy = (RecyclerView) findViewById(R.id.latest_wall_recycler);
        trending_recy = (RecyclerView) findViewById(R.id.trending_wall_recycler);

        categoryData();
        latestwall();
        trendingwall();
        bannerload();

        imageButton = findViewById(R.id.basin_image_view);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawer.openDrawer(Gravity.LEFT);
            }
        });
        img2 = findViewById(R.id.basin_image_view2);
        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(

                        Intent.ACTION_SEND);

                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_SUBJECT, "Wallpaper Store 2021");
                i.putExtra(

                        Intent.EXTRA_TEXT, "I have found a 3D wallpaper app.\n" +
                                "You can also Download a plenty no. of 3D wallpaper for free.  \n" +
                                "Click Below to download this Awesome app\n" +
                                "https://play.google.com/store/apps/details?id=com.inmortal.wallpaperstore");

                startActivity(Intent.createChooser(

                        i,

                        "Share this to your friends..."));
            }
        });
    }

    private void trendingwall() {
        networkCall.NetworkAPICall(ApiURL.trending_wall, true);

    }

    private void latestwall() {
        networkCall.NetworkAPICall(ApiURL.latest_wall, true);

    }

    private void categoryData() {
        networkCall.NetworkAPICall(ApiURL.category_list, true);

    }

    private void bannerload() {
        networkCall.NetworkAPICall(ApiURL.Banner_main, true);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();


        if (id == R.id.about) {

//            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://inmortaltechnologies.com/about.html")));
//            Intent about = new Intent(MainActivity.this, About.class);
//            startActivity(about);
        } else if (id == R.id.rate) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.inmortal.wallpaperstore")));
        } else if (id == R.id.refresh) {


        } else if (id == R.id.contactUs) {
//            // startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://inmortaltechnologies.com/Contact.html")));
//            Intent contact = new Intent(MainActivity.this, contact.class);
//            startActivity(contact);
        }
        mDrawer.closeDrawer(GravityCompat.START);
        return false;
    }

    @Override
    public void viewPageImageClick(String page, int position) {

    }

    @Override
    public Builders.Any.B getAPIB(String apitype) {
        Builders.Any.B ion = null;
        switch (apitype) {
            case ApiURL.Banner_main:
                ion = (Builders.Any.B) Ion.with(MainActivity.this)
                        .load("GET", ApiURL.Banner_main);
                break;

            case ApiURL.category_list:
                ion = (Builders.Any.B) Ion.with(MainActivity.this)
                        .load("GET", ApiURL.category_list);

                break;

            case ApiURL.trending_wall:
                ion = (Builders.Any.B) Ion.with(MainActivity.this)
                        .load("GET", ApiURL.trending_wall);

                break;

            case ApiURL.latest_wall:
                ion = (Builders.Any.B) Ion.with(MainActivity.this)
                        .load("GET", ApiURL.latest_wall);

                break;
        }
        return ion;
    }

    @Override
    public void SuccessCallBack(JSONObject jsonstring, String apitype) throws JSONException {
        switch (apitype) {
            case ApiURL.Banner_main:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String success = jsonObject.getString("success");
                    if (success.equals("true")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            bannerpojo = new Gson().fromJson(jsonArray.optJSONObject(i).toString(), Bannerpojo.class);
                            listBanners.add(bannerpojo);
                        }

                        ViewPager viewPagerBanners = (ViewPager) findViewById(R.id.main_grid_vf_banner);
                        viewPagerBanners.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                            @Override
                            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                                for (int i = 0; i < listBanners.size(); i++) {

                                }

                            }

                            @Override
                            public void onPageSelected(int position) {

                            }

                            @Override
                            public void onPageScrollStateChanged(int state) {

                            }
                        });

                        Adapter_main_viewPager viewPagerAdapter = new Adapter_main_viewPager(this, listBanners, this, "offer");
                        viewPagerBanners.setAdapter(viewPagerAdapter);
                        viewPagerAdapter.notifyDataSetChanged();

                    } else {

                        progress.dismiss();
                    }
                } catch (JSONException e1) {
                    Toast.makeText(MainActivity.this, "" + e1, Toast.LENGTH_SHORT).show();
                }

                break;

            case ApiURL.category_list:
                try {
                    JSONObject jsonObject = new JSONObject(jsonstring.toString());
                    String status = jsonObject.getString("success");
                    String msg = jsonObject.getString("message");


                    if (status.equals("true")) {

                        JSONArray jsonObject1 = jsonObject.getJSONArray("data");

                        for (int i = 0; i < jsonObject1.length(); i++) {
                            model_category_list = new Gson().fromJson(jsonObject1.optJSONObject(i).toString(), Model_category_list.class);
                            arrCategory_list.add(model_category_list);

                        }


                        adapter_category = new Adapter_category(arrCategory_list, MainActivity.this, R.layout.category_layout, this, 1);
                        category_recy.setHasFixedSize(true);
                        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
                        category_recy.setLayoutManager(layoutManager);
                        category_recy.setAdapter(adapter_category);


                    } else {


                        String fail_status = jsonObject.getString("success");

                        if (fail_status.equals("false")) {

                            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Wallpaper Store")
                                    .setContentText(msg)
                                    .show();


                            //  Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                        }
                        progress.dismiss();
                    }
                } catch (JSONException e1) {


                    Toast.makeText(MainActivity.this, "" + e1, Toast.LENGTH_SHORT).show();
                }

                break;

            case ApiURL.latest_wall:
                try {
                    JSONObject jsonObjectLatestwall = new JSONObject(jsonstring.toString());
                    String status = jsonObjectLatestwall.getString("success");
                    String msg = jsonObjectLatestwall.getString("message");


                    if (status.equals("true")) {

                        JSONArray jsonObjectLatestarr = jsonObjectLatestwall.getJSONArray("data");

                        for (int i = 0; i < jsonObjectLatestarr.length(); i++) {
                            model_latest_wall = new Gson().fromJson(jsonObjectLatestarr.optJSONObject(i).toString(), Model_latest_wall.class);
                            arrLatestwall.add(model_latest_wall);

                        }


                        adapter_latest_wall = new Adapter_latest_wall(arrLatestwall, MainActivity.this, R.layout.latest_wall_layout, this, 1);
                        latest_recy.setHasFixedSize(true);
                        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
                        latest_recy.setLayoutManager(layoutManager);
                        latest_recy.setAdapter(adapter_latest_wall);


                    } else {


                        String fail_status = jsonObjectLatestwall.getString("success");

                        if (fail_status.equals("false")) {

                            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Wallpaper Store")
                                    .setContentText(msg)
                                    .show();


                            //  Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                        }
                        progress.dismiss();
                    }
                } catch (JSONException e1) {


                    Toast.makeText(MainActivity.this, "" + e1, Toast.LENGTH_SHORT).show();
                }

                break;

            case ApiURL.trending_wall:
                try {
                    JSONObject jsonObjecttrendingl = new JSONObject(jsonstring.toString());
                    String status = jsonObjecttrendingl.getString("success");
                    String msg = jsonObjecttrendingl.getString("message");


                    if (status.equals("true")) {

                        JSONArray jsonObjectTrendarr = jsonObjecttrendingl.getJSONArray("data");

                        for (int i = 0; i < jsonObjectTrendarr.length(); i++) {
                            model_trending_wall = new Gson().fromJson(jsonObjectTrendarr.optJSONObject(i).toString(), Model_trending_wall.class);
                            arrTrending.add(model_trending_wall);

                        }


                        adapter_trending_wall = new Adapter_Trending_wall(arrTrending, MainActivity.this, R.layout.latest_wall_layout, this, 1);
                        trending_recy.setHasFixedSize(true);
                        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
                        trending_recy.setLayoutManager(layoutManager);
                        trending_recy.setAdapter(adapter_trending_wall);


                    } else {


                        String fail_status = jsonObjecttrendingl.getString("success");

                        if (fail_status.equals("false")) {

                            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText("Wallpaper Store")
                                    .setContentText(msg)
                                    .show();


                            //  Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                        }
                        progress.dismiss();
                    }
                } catch (JSONException e1) {


                    Toast.makeText(MainActivity.this, "" + e1, Toast.LENGTH_SHORT).show();
                }

                break;
        }


    }

    @Override
    public void ErrorCallBack(String jsonstring, String apitype) {

    }

    @Override
    public void getAdapterView(View view, List objects, int position, int from) {
        Model_category_list model_category_list = arrCategory_list.get(position);
        TextView name = view.findViewById(R.id.category_txt);
        ImageView pic = view.findViewById(R.id.category_img);
        LinearLayout category = view.findViewById(R.id.category);
        category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Category_id = String.valueOf(model_category_list.getId());


                Intent intent = new Intent(MainActivity.this, catrgory_wallpapers.class);
                intent.putExtra("id", Category_id);
                startActivity(intent);
            }
        });

        model_category_list.getCategory();
        String category_name;
        category_name = model_category_list.getCategory_name();
        name.setText(category_name);
        String img;
        img = model_category_list.getCategory();
        if (!img.equals("")) {
            Picasso.with(getApplicationContext())
                    .load(img)
                    .into(pic);
        }

    }

    public void more_category(View view) {
        Intent intent = new Intent(MainActivity.this, more_wallpaper_category.class);
        startActivity(intent);
    }

    @Override
    public void getLatestwall(View view, List objects, int position, int from) {
        model_latest_wall = arrLatestwall.get(position);

        ImageView pic = view.findViewById(R.id.latest_image_view);
        LinearLayout category = view.findViewById(R.id.latest_category);
        category.setOnClickListener(v -> {


            Drawable drawable = pic.getDrawable();
            //or if the above line of code doesn't work try fetching the image from your array list
            arrLatestwall.get(position);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Dialog dialogg = new Dialog(MainActivity.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
            dialogg.setContentView(R.layout.activity_full_iimage);
            CropImageView Full = dialogg.findViewById(R.id.iv_background);
            Full.setImageBitmap(bitmap);

            CircleImageView setWall = dialogg.findViewById(R.id.button_setwallpaper);
            setWall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(MainActivity.this, "Please Wait....", Toast.LENGTH_SHORT).show();

                    wal_set = Full.getCroppedImage();
                    WallpaperManager wallpaperManager = WallpaperManager.getInstance(getApplicationContext());

                    wallpaperManager.setWallpaperOffsetSteps(0, 0);
                    try {

                        wallpaperManager.setBitmap(wal_set);
                        Toast.makeText(MainActivity.this, "Wallpaper set Successfully", Toast.LENGTH_SHORT).show();


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    dialogg.dismiss();
                }
            });

            Window window = dialogg.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();
            wlp.gravity = Gravity.CENTER;

            dialogg.show();
            dialogg.setCanceledOnTouchOutside(true);
        });

        model_latest_wall.getWallpaper();

        String img;
        img = model_latest_wall.getWallpaper();
        if (!img.equals("")) {
            Picasso.with(getApplicationContext())
                    .load(img)
                    .into(pic);


        }

    }

    public void more_Latest(View view) {
        Intent intent2 = new Intent(MainActivity.this, Latest_wallpaper.class);
        startActivity(intent2);
    }

    @Override
    public void getTrendingwall(View view, List objects, int position, int from) {
        model_trending_wall = arrTrending.get(position);

        ImageView pic = view.findViewById(R.id.latest_image_view);
        LinearLayout category = view.findViewById(R.id.latest_category);
        category.setOnClickListener(v -> {


            Drawable drawable = pic.getDrawable();
            //or if the above line of code doesn't work try fetching the image from your array list
            arrTrending.get(position);
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            Dialog dialogg = new Dialog(MainActivity.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
            dialogg.setContentView(R.layout.activity_full_iimage);
            CropImageView Full = dialogg.findViewById(R.id.iv_background);
            Full.setImageBitmap(bitmap);

            CircleImageView setWall = dialogg.findViewById(R.id.button_setwallpaper);
            setWall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(MainActivity.this, "Please Wait....", Toast.LENGTH_SHORT).show();

                    wal_set = Full.getCroppedImage();
                    WallpaperManager wallpaperManager = WallpaperManager.getInstance(getApplicationContext());

                    wallpaperManager.setWallpaperOffsetSteps(0, 0);
                    try {

                        wallpaperManager.setBitmap(wal_set);
                        Toast.makeText(MainActivity.this, "Wallpaper set Successfully", Toast.LENGTH_SHORT).show();


                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    dialogg.dismiss();
                }
            });

            Window window = dialogg.getWindow();
            WindowManager.LayoutParams wlp = window.getAttributes();
            wlp.gravity = Gravity.CENTER;

            dialogg.show();
            dialogg.setCanceledOnTouchOutside(true);


        });



        String img;
        img = model_trending_wall.getWallpaper();
        if (!img.equals("")) {
            Picasso.with(getApplicationContext())
                    .load(img)
                    .into(pic);


        }
    }

    public void more_Trending(View view) {
        Intent moretrend = new Intent(MainActivity.this, more_trending.class);
        startActivity(moretrend);
    }
}