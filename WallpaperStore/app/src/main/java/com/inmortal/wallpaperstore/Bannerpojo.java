package com.inmortal.wallpaperstore;
public class Bannerpojo
{
    private int id;

    private String banner;

    private String created_at;

    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return this.id;
    }
    public void setBanner(String banner){
        this.banner = banner;
    }
    public String getBanner(){
        return this.banner;
    }
    public void setCreated_at(String created_at){
        this.created_at = created_at;
    }
    public String getCreated_at(){
        return this.created_at;
    }
}
