package com.inmortal.wallpaperstore;

public class Model_trending_wall {
    private int id;

    private String wallpaper;

    private String trend;

    private String created_at;

    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return this.id;
    }
    public void setWallpaper(String wallpaper){
        this.wallpaper = wallpaper;
    }
    public String getWallpaper(){
        return this.wallpaper;
    }
    public void setTrend(String trend){
        this.trend = trend;
    }
    public String getTrend(){
        return this.trend;
    }
    public void setCreated_at(String created_at){
        this.created_at = created_at;
    }
    public String getCreated_at(){
        return this.created_at;
    }
}

