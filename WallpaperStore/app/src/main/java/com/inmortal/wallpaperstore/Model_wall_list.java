package com.inmortal.wallpaperstore;
public class Model_wall_list
{
    private int id;

    private String category_id;

    private String wallpaper;

    private String created_at;

    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return this.id;
    }
    public void setCategory_id(String category_id){
        this.category_id = category_id;
    }
    public String getCategory_id(){
        return this.category_id;
    }
    public void setWallpaper(String wallpaper){
        this.wallpaper = wallpaper;
    }
    public String getWallpaper(){
        return this.wallpaper;
    }
    public void setCreated_at(String created_at){
        this.created_at = created_at;
    }
    public String getCreated_at(){
        return this.created_at;
    }
}
