package com.inmortal.wallpaperstore.utill;

public class ApiURL {

    public final static String main_url = "http://inmortaltech.com/Wallpaper-APIs/public/api/";
    public final static String category_list = main_url+"category";
    public final static String wallpaper_list = main_url+"wallpaper_list";
    public final static String category_wallpaper_list = main_url+"wallpaper";
    public final static String Banner_main = main_url+"banner";
    public final static String latest_wall = main_url+"latest_wallpaper";
    public final static String trending_view = main_url+"view_wallpaper";
    public final static String trending_wall = main_url+"trend_wallpaper";

}
